Thinq Code Challenge:

**Backend**:

The API was developed using NodeJs with ExpressJS, Nodemon and Mocha/Chai for unit testing. The API uses the TMDB Public API to fetch the data.

The project requires Node v14 to start, it uses ES modules which are available without Babel from that version.

To start the project get a V4 API Key from TMDB, 'create a config.js file in the config folders and a .Env file in the root, there are default files with the required keys in each folder, after that do `cd backend` `npm i` and `npm start`

To run the tests do `npm test`

**Frontend**:

Developed using ReactJS with Create-React-App which uses Jest and React-Testing-Library for unit testing.

Used react-bootstrap to create a responsive view

To start the project create a config.js file in the config folder, do `cd thinq-frontend` `npm i` and `npm start`

To run the tests do `npm test`

**Notes**:
    The backend has 2 EPs, a Get Popular Movies Endpoint and a Search Movies EP, the idea was to use react-router to develop different routes, and be able to use the search EP, but because of time constraints i wasn't able to dedicate enough time to fully develop the frontend.