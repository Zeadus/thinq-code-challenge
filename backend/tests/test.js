import mocha from 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../app.js';

chai.use(chaiHttp);

const { expect } = chai;

mocha.describe('Get movies', () => {
  it('should return popular movies and at least 1 movie', (done) => {
    chai.request(app)
      .get('/movies/popular')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.movies.results).to.be.a('array');
        expect(res.body.movies.results.length).to.be.above(0);
        done();
      });
  });

  it('should return at least 1 movie', (done) => {
    chai.request(app)
      .get('/movies/search?search=a')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.movies.results).to.be.a('array');
        expect(res.body.movies.results.length).to.be.above(0);
        done();
      });
  });
});
