import { Router } from 'express';
import MoviesController from '../controllers/movie.js';

const router = Router();

router.get('/popular', MoviesController.getPopularMovies);
router.get('/search', MoviesController.searchMovies);
export default router;
