import { Router } from 'express';
import MovieRoutes from './movieRoutes.js';

const router = Router();

router.use('/movies', MovieRoutes);

export default router;
