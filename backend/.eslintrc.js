module.exports = {
  env: {
    es6: true,
    node: true,
    mocha: true
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  rules: {
    'comma-dangle': 0,
    'no-console': 0,
    'arrow-body-style': 0,
    'linebreak-style': ['error', 'windows'],
    'import/extensions': 'off'
  },
};
