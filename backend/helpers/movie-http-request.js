import gaxios from 'gaxios';
import configJson from '../config/config.js';

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
const config = configJson[env];

gaxios.instance.defaults = {
  baseURL: config.tmdbURL,
  headers: {
    Authorization: `Bearer ${config.tmdbApiKey}`,
    'Content-Type': 'application/json;charset=utf-8'
  }
};

export const get = async (route, params) => {
  try {
    const res = await gaxios.request({ url: `/${route}`, params, method: 'GET' });
    return res.data;
  } catch (err) {
    throw err.response;
  }
};

export const post = async (route, body) => {
  try {
    const res = await gaxios.request({ url: `/${route}`, data: body, method: 'POST' });
    return res.data;
  } catch (err) {
    throw err.response;
  }
};
