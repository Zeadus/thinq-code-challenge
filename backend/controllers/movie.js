import movieService from '../services/movieService.js';

const getPopularMovies = async (req, res) => {
  try {
    const movies = await movieService.getPopularMovies(req.query);
    return res.status(200).json({ status: 200, movies }).end();
  } catch (err) {
    return res.status(err.status).json(err).end();
  }
};

const searchMovies = async (req, res) => {
  try {
    const movies = await movieService.searchMovies(req.query);
    return res.status(200).json({ status: 200, movies }).end();
  } catch (err) {
    return res.status(err.status).json(err).end();
  }
};

export default { getPopularMovies, searchMovies };
