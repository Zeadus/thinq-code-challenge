import { get } from '../helpers/movie-http-request.js';

const getPopularMovies = async (query) => {
  try {
    const res = await get('movie/popular', query);
    res.status = 200;
    return res;
  } catch (e) {
    const error = {
      status: e.status,
      error: e.data.status_message
    };
    throw error;
  }
};

const searchMovies = async (query) => {
  try {
    const res = await get('search/movie', query);
    return res;
  } catch (e) {
    const error = {
      status: e.status,
      error: e.data.status_message
    };
    throw error;
  }
};

export default { getPopularMovies, searchMovies };
