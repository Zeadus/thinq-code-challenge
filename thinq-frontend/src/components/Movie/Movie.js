import React from 'react';
import { Image, OverlayTrigger, Tooltip } from 'react-bootstrap';
import config from '../../config/config';
import './Movie.css';

const RenderTooltip = (props) => {
    return (
        <Tooltip id="button-tooltip">
            {props}
        </Tooltip>
    );
} 

const Movie = (props) => {
    return (
        <OverlayTrigger placement="bottom" delay={{ show:0, hide:150 }} overlay={RenderTooltip(props.movie.title)}>    
            <a className={props.index !== 0 && props.index % 4 === 0 ? 'row-space' : ''} target="_blank"  rel="noopener noreferrer" href = {config.TMDBMovieUrl + props.movie.id}>
                <span className="rating">{props.movie.vote_average * 10}%</span>
                <Image 
                    fluid 
                    src = {config.TMDBImgUrl + props.movie.poster_path} 
                    alt={props.movie.title}>
                </Image>
            </a>
        </OverlayTrigger> 
    );
}

export default Movie;
