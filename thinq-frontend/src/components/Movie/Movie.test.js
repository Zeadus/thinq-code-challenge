import React from 'react';
import { render, screen } from '@testing-library/react';
import Movie from './movie';
import movieJson from '../../json-mock/movie'
test('renders movie', async () => {

  render(<Movie movie={movieJson}/>);

  expect(screen.getByAltText('Enola Holmes')).toBeInTheDocument();

});
