import React, { useEffect, useState } from 'react';
import { Button, Row } from 'react-bootstrap';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import config from '../config/config';
import './App.css';
import MovieList from './MovieList/MovieList';
import Toolbar from './Toolbar/Toolbar';

const App = () => {
  const [movies, setMovies] = useState([]);
  const [moviesPage, setMoviesPage] = useState(1);
  const [totalMoviesPage, setTotalMoviesPage] = useState(0);
  const [isLoading, setLoading] = useState(false);

  const getPopularMovies = async () => {
    setLoading(true);
    const res = await fetch(
      `${config.apiUrl}/movies/popular?${moviesPage > 1 ? `page=${moviesPage}` : ''}`,
      {
        method: 'GET',
        headers: new Headers({
          Accept: "application/vnd.github.cloak-preview"
        })
      }
    );
    const resJson = await res.json();
    await setMovies([...movies, ...resJson.movies.results]);
    await setTotalMoviesPage(resJson.movies.total_pages);
    setLoading(false);
  }
  
  useEffect(() => {
    getPopularMovies();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [moviesPage]);

  

  return (
    <div className="App">
      <Container fluid>
        <Row>
          <Col xs={12}>
            <Toolbar></Toolbar>
          </Col>
        </Row>
        <Row>
          <Col xs={0} md={3}>
          </Col>
          <Col xs={12} md={6}>
            <Row role = "movie-list">
              <MovieList movies={movies}></MovieList>
            </Row>
            { totalMoviesPage > moviesPage &&
              <Row className = "next-page-row">
                <Col>
                  <Button onClick={() => setMoviesPage(moviesPage + 1)} disabled={isLoading} block variant="info">
                    Load More Movies
                  </Button>
                </Col>
              </Row>
            }
            <Row>

            </Row>
          </Col>
          <Col xs={0} md={3}>
          </Col>
        </Row>
      </Container> 
    </div>
  );
}

export default App;
