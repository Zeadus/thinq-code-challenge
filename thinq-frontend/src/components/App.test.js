import React from 'react';
import { render, waitForElement, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import App from './App';
import moviesJson from '../json-mock/moviesList';

test('renders App and movies list without pagination', async () => {

  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(moviesJson)
    })
  );

  render(<App />);

  await waitForElement(() => screen.getByRole('movie-list'));

  expect(screen.getByRole('movie-list')).toBeInTheDocument();
  expect(screen.getByAltText('Enola Holmes')).toBeInTheDocument();
  expect(screen.queryByText(/load more movies/i)).not.toBeInTheDocument();
  global.fetch.mockRestore();
});

test('renders App and movies list with pagination', async () => {
  const moviesJsonPag = {...moviesJson, movies: {...moviesJson.movies, total_pages: 2 }};
  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(moviesJsonPag)
    })
  );

  render(<App />);

  await waitForElement(() => screen.getByRole('movie-list'));

  expect(screen.getByRole('movie-list')).toBeInTheDocument();
  expect(screen.getByAltText('Enola Holmes')).toBeInTheDocument();
  expect(screen.queryByText(/load more movies/i)).toBeInTheDocument();
  global.fetch.mockRestore();
});
