import React from 'react';
import { render, screen } from '@testing-library/react';
import MovieList from './MovieList';
import moviesJson from '../../json-mock/moviesList'
test('Renders Movie List', async () => {
  render(<MovieList movies={moviesJson.movies.results}/>);
  
  expect(screen.getByAltText('Enola Holmes')).toBeInTheDocument();

});
