import React from 'react';
import { Col } from 'react-bootstrap';
import Movie from '../Movie/Movie';
import './MovieList.css';

const MovieList = (props) => {

    const movies = props.movies.map((movie, index) => 
        (
            <Col key={movie.id} role="movie-container" className="movie-container" xs={3}>
                <Movie movie={movie} index={index+1}></Movie>
            </Col>
        )
    );

    return (
        movies
    )
}

export default MovieList;
