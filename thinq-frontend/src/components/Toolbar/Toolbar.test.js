import React from 'react';
import { render, screen, waitForElement } from '@testing-library/react';
import Toolbar from './Toolbar';

test('renders toolbar', async () => {
  render(<Toolbar />);
  await waitForElement(() => screen.getByRole('navigation'));
  expect(screen.getByRole('navigation')).toBeInTheDocument();

});
