import React from 'react';
import { Navbar } from 'react-bootstrap';
import './Toolbar.css';

const Toolbar = (props) => {
  return (
    <div className="navbar-container"> 
        <Navbar variant="dark">
            <Navbar.Brand>ThinQing About Movies</Navbar.Brand>
        </Navbar> 
    </div>
  );
}

export default Toolbar;
